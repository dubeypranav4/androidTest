package com.pranav.dubey.androidtest;

import android.provider.BaseColumns;

/**
 * Created by dubeypranav4 on 17-03-2017.
 */
public final class UserDatabaseContract {

    public UserDatabaseContract(){
    }

    public static abstract class UserDetails implements BaseColumns{
        public static final String TABLE_NAME="userDetails";
        public static final String COLUMN_ID="_id";
        public static final String COLUMN_NAME_FIRST_NAME="firstName";
        public static final String COLUMN_NAME_MIDDLE_NAME="middleName";
        public static final String COLUMN_NAME_LAST_NAME="lastName";
        public static final String COLUMN_NAME_MOBILE_NUMBER="mobileNumber";
        public static final String COLUMN_NAME_EMAIL="email";
        public static  final String DEFAULT_VALUE="NS";
        public static final String PROGRAM_NAME="Program Name";
        public static final String SELECTED_ATTENDEE_VALUE="AS";
        public static final String COLUMN_NAME_PASSWORD="password";
    }
}
