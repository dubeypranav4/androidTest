package com.pranav.dubey.androidtest;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.ListActivity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.Toast;
import com.pranav.dubey.androidtest.UserDatabaseContract.UserDetails;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class AddProgramActivity extends AppCompatActivity {
ListView listView;
    private View mLoginFormView;
    private View mProgressView;
    EditText mProgramTitleView;
    Button mCreateProgram;
    AddProgramColumn mAddProgramTask=null;
public Cursor c=null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_program);
        listView= (ListView) findViewById(R.id.user_list);
        mLoginFormView=findViewById(R.id.program_form);
        mProgressView=findViewById(R.id.add_program_progress);
        mCreateProgram= (Button) findViewById(R.id.createProgram);
        mProgramTitleView= (EditText) findViewById(R.id.programTitle);

       getData();

    }

    private void getData() {
        showProgress(true);
        GetTable getTable=new GetTable();
        getTable.execute((Void[]) null);
    }

    public class GetTable extends AsyncTask<Void,Void,Cursor>{

        UserDatabaseDbHelper mDbHelper = new UserDatabaseDbHelper(getApplicationContext());
        @Override
        protected Cursor doInBackground(Void... voids) {
            SQLiteDatabase db = mDbHelper.getReadableDatabase();
            String[] projection = {
                    UserDatabaseContract.UserDetails.COLUMN_ID,
                    UserDatabaseContract.UserDetails.COLUMN_NAME_FIRST_NAME,
                    UserDatabaseContract.UserDetails.COLUMN_NAME_MIDDLE_NAME,
                    UserDatabaseContract.UserDetails.COLUMN_NAME_LAST_NAME,
                    UserDatabaseContract.UserDetails.COLUMN_NAME_MOBILE_NUMBER,
                    UserDatabaseContract.UserDetails.COLUMN_NAME_EMAIL,
                    UserDatabaseContract.UserDetails.COLUMN_NAME_PASSWORD

            };
            Cursor cursor= db.query(
                    UserDatabaseContract.UserDetails.TABLE_NAME,
                    projection,
                    null,
                    null,
                    null,
                    null,
                    null

            );
            return cursor;
        }

        @Override
        protected void onPostExecute(Cursor cursor) {
            if(cursor.getCount()>0) {
                populateListView(cursor);
            }
            else{
                Toast.makeText(getApplicationContext(),"ye to khali hai",Toast.LENGTH_LONG).show();
            }
            showProgress(false);

        }
    }

    private void populateListView(Cursor cursor) {
try {
    final CustomArrayAdapter customArrayAdapter = new CustomArrayAdapter(AddProgramActivity.this, cursor,cursor.getCount());
    listView.setAdapter(customArrayAdapter);
    mCreateProgram.setVisibility(View.VISIBLE);
    mCreateProgram.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            AttendeesList attendeesList=customArrayAdapter.getRegisteredAttendees();
            if(attendeesList.size()>0){
                createTableForProgram(getIntent().getStringExtra(UserDatabaseContract.UserDetails.COLUMN_NAME_EMAIL),
                        getIntent().getStringExtra(UserDatabaseContract.UserDetails.COLUMN_NAME_PASSWORD),
                        attendeesList);
            }else{
                Toast.makeText(getApplicationContext(),"Khali Hai",Toast.LENGTH_LONG).show();
            }
        }
    });

}catch (Exception e){
    e.printStackTrace();
}

    }

    private void createTableForProgram(String email, String password, AttendeesList attendeesList) {
        String programTitle=mProgramTitleView.getText().toString().trim();
        mProgramTitleView.setError(null);
        if(TextUtils.isEmpty(programTitle)){
            mProgramTitleView.setError("This Field is required");
            mProgramTitleView.requestFocus();

        }
        else if(!(programTitle.matches("[a-zA-Z0-9 ]*"))){
            mProgramTitleView.setError("Special characters not allowed");
            mProgramTitleView.requestFocus();
        }
         else{
            showProgress(true);
            programTitle=programTitle.replace(" ","_");
            mAddProgramTask=new AddProgramColumn(programTitle,attendeesList);
            mAddProgramTask.execute((Void[]) null);

        }
    }
public  class AddProgramColumn extends AsyncTask<Void,Void,Boolean>{

   final String mProgramTitle;
    final AttendeesList mAttendeesList;
    AddProgramColumn(String programTitle,AttendeesList attendeesList){
        mProgramTitle=programTitle.toUpperCase();
        mAttendeesList=attendeesList;
    }
    UserDatabaseDbHelper mDbHelper = new UserDatabaseDbHelper(getApplicationContext());

    @Override
    protected Boolean doInBackground(Void... voids) {// 0 for exceptions -1 for false and 1 for sucess

            SQLiteDatabase db1=mDbHelper.getWritableDatabase();
            try {
                String alterQuery="ALTER TABLE "+UserDetails.TABLE_NAME+" ADD COLUMN "+ mProgramTitle +" TEXT DEFAULT \'"+UserDetails.DEFAULT_VALUE+"\'";db1.execSQL(alterQuery);
                ContentValues values=new ContentValues();
                values.put(mProgramTitle,UserDetails.SELECTED_ATTENDEE_VALUE);
                String selection=UserDetails.COLUMN_NAME_EMAIL+" LIKE ?";

                for(int i=0;i<mAttendeesList.size();i++){

                    db1.update(UserDetails.TABLE_NAME,
                            values,
                            selection,
                            new String[] {mAttendeesList.getDataAtI(i)}
                            );
                }

            }catch (Exception e){
                e.printStackTrace();
                return false;
            }
            return true;

    }

    @Override
    protected void onPostExecute(Boolean success) {
        mAddProgramTask=null;
        showProgress(false);
        if(success){
            Toast.makeText(getApplicationContext(), "Succesfully created a program", Toast.LENGTH_LONG).show();
            String fileName=getIntent().getStringExtra(UserDetails.COLUMN_NAME_EMAIL)+".txt";
            File file=new File(getApplicationContext().getFilesDir(),fileName);
            if(!file.exists()){
                try {
                    file.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            FileOutputStream outputStream;
            try {
                outputStream=openFileOutput(fileName, Context.MODE_APPEND);
                outputStream.write("\n".getBytes());
                outputStream.write(mProgramTitle.getBytes());
                outputStream.close();

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            //// Todo: remove this block of code
            String ret="";
            InputStream inputStream= null;
            try {
                inputStream = getApplicationContext().openFileInput(fileName);
                if(inputStream!=null){
                    InputStreamReader inputStreamReader=new InputStreamReader(inputStream);
                    BufferedReader bufferedReader=new BufferedReader(inputStreamReader);
                    String recieveString="";
                    StringBuilder stringBuilder=new StringBuilder();
                    while((recieveString=bufferedReader.readLine())!=null){
                        stringBuilder.append("|"+recieveString);
                    }
                    inputStream.close();
                    ret=stringBuilder.toString();

                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.d("file"+fileName,ret);
        }
        else{
           // Toast.makeText(getApplicationContext(),"Exception occured see logcat",Toast.LENGTH_LONG).show();
            mProgramTitleView.setError("Program name already exists");
            mProgramTitleView.requestFocus();
        }
    }
}
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }
}
