package com.pranav.dubey.androidtest;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;
import com.pranav.dubey.androidtest.UserDatabaseContract.UserDetails;

public class UserProfilePage extends AppCompatActivity {
TextView mNameView,mEmailView,mMobileView;
    public String mFirstName, mMiddleName, mLastName, mFullName,mEmail, mMobileNumber, mPassword;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile_page);
        mNameView= (TextView) findViewById(R.id.name);
        mEmailView= (TextView) findViewById(R.id.email_add);
        mMobileView= (TextView) findViewById(R.id.mobileNumber);

       mFirstName=getIntent().getStringExtra(UserDetails.COLUMN_NAME_FIRST_NAME);
        mMiddleName=getIntent().getStringExtra(UserDetails.COLUMN_NAME_MIDDLE_NAME);
        mLastName=getIntent().getStringExtra(UserDetails.COLUMN_NAME_LAST_NAME);
        mEmail=getIntent().getStringExtra(UserDetails.COLUMN_NAME_EMAIL);
        mMobileNumber=getIntent().getStringExtra(UserDetails.COLUMN_NAME_MOBILE_NUMBER);
        mPassword=getIntent().getStringExtra(UserDetails.COLUMN_NAME_PASSWORD);
        if(TextUtils.isEmpty(mMiddleName)) {
            mFullName = mFirstName + " " + mLastName;
        }
        else{
            mFullName=mFirstName+" "+mMiddleName+" "+mLastName;
        }
        mNameView.setText(mFullName);
        mEmailView.setText(mEmail);
        mMobileView.setText(mMobileNumber);

    }
    public void logOut(View view){
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.clear();
        editor.commit();
        Intent intent=new Intent(getApplicationContext(),LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }
    public void onEditClicked(View view){
            Intent intent=new Intent(getApplicationContext(),EditUserData.class);
        intent.putExtra(UserDetails.COLUMN_NAME_FIRST_NAME,mFirstName);
        intent.putExtra(UserDetails.COLUMN_NAME_MIDDLE_NAME,mMiddleName);
        intent.putExtra(UserDetails.COLUMN_NAME_LAST_NAME, mLastName);
        intent.putExtra(UserDetails.COLUMN_NAME_MOBILE_NUMBER, mMobileNumber);
        intent.putExtra(UserDetails.COLUMN_NAME_EMAIL, mEmail);
        intent.putExtra(UserDetails.COLUMN_NAME_PASSWORD, mPassword);
        startActivity(intent);
        finish();
    }
    public void deactivateClicked(View view){
        AlertDialog.Builder builder=new AlertDialog.Builder(this);
        builder.setTitle("Alert")
                .setMessage("You are about to deactivate your account.All your information will be deleted.Are you sure?")
                .setPositiveButton("I KNOW", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        DeactivateAccount mDeactTask=new DeactivateAccount(mEmail);
                        mDeactTask.execute((Void[]) null);
                    }
                })
                .setNegativeButton("TAKE ME BACK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //do nothing
                    }
                }).show();
    }
    public class DeactivateAccount extends AsyncTask<Void,Void,Boolean>{
private final String mEmail;

        DeactivateAccount(String email){
            mEmail=email;
        }
        UserDatabaseDbHelper mDbHelper=new UserDatabaseDbHelper(getApplicationContext());
        @Override
        protected Boolean doInBackground(Void... voids) {
            SQLiteDatabase db = mDbHelper.getWritableDatabase();
            db.delete(UserDetails.TABLE_NAME,UserDetails.COLUMN_NAME_EMAIL+" = ?",
                    new String[] {String.valueOf(mEmail)});
            db.close();
            mDbHelper.close();
            return true;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
            SharedPreferences.Editor editor = pref.edit();
            editor.clear();
            editor.commit();
            Intent intent=new Intent(getApplicationContext(),LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        }
    }
    public void addProgramClicked(View view){
        Intent intent=new Intent(getApplicationContext(),AddProgramActivity.class);
        intent.putExtra(UserDetails.COLUMN_NAME_EMAIL,getIntent().getStringExtra(UserDetails.COLUMN_NAME_EMAIL));
        intent.putExtra(UserDetails.COLUMN_NAME_PASSWORD,getIntent().getStringExtra(UserDetails.COLUMN_NAME_PASSWORD));
        startActivity(intent);
    }
    public void myProgramsClicked(View view){
        Intent intent=new Intent(getApplicationContext(),UserProgramList.class);
        intent.putExtra(UserDetails.COLUMN_NAME_EMAIL,getIntent().getStringExtra(UserDetails.COLUMN_NAME_EMAIL));
        startActivity(intent);
    }
}
