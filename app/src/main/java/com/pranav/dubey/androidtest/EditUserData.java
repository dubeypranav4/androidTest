package com.pranav.dubey.androidtest;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

public class EditUserData extends AppCompatActivity {
    public String mPrevFirstName, mPrevMiddleName, mPrevLastName,mPrevEmail, mPrevMobileNumber, mPrevPassword;
    EditText mFirstName, mMiddleName, mLastName, mMobileNumber, mPassword;
    Button mSave;
    private UserEditTask mEditTask=null;
    private View mProgressView;
    private View mLoginFormView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_user_data);
        mPrevFirstName=getIntent().getStringExtra(UserDatabaseContract.UserDetails.COLUMN_NAME_FIRST_NAME);
        mPrevMiddleName=getIntent().getStringExtra(UserDatabaseContract.UserDetails.COLUMN_NAME_MIDDLE_NAME);
        mPrevLastName=getIntent().getStringExtra(UserDatabaseContract.UserDetails.COLUMN_NAME_LAST_NAME);
        mPrevEmail=getIntent().getStringExtra(UserDatabaseContract.UserDetails.COLUMN_NAME_EMAIL);
        mPrevMobileNumber=getIntent().getStringExtra(UserDatabaseContract.UserDetails.COLUMN_NAME_MOBILE_NUMBER);
        mPrevPassword=getIntent().getStringExtra(UserDatabaseContract.UserDetails.COLUMN_NAME_PASSWORD);

        mFirstName= (EditText) findViewById(R.id.editFirstName);
        mMiddleName= (EditText) findViewById(R.id.editMiddleName);
        mLastName= (EditText) findViewById(R.id.editLastName);
        mMobileNumber= (EditText) findViewById(R.id.edit_mobile_number);
        mPassword= (EditText) findViewById(R.id.edit_password);
        mSave= (Button) findViewById(R.id.save_button);
        mProgressView=findViewById(R.id.edit_user_progress);
        mLoginFormView=findViewById(R.id.edit_form);

        mFirstName.setText(mPrevFirstName);
        mMiddleName.setText(mPrevMiddleName);
        mLastName.setText(mPrevLastName);
        mMobileNumber.setText(mPrevMobileNumber);
        mPassword.setText(mPrevPassword);

        mSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                mgr.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                attemptEdit();
            }
        });

    }
public class UserEditTask extends AsyncTask<Void,Void,Boolean>{
    private final String mFirstName, mMiddleName, mLastName, mEmail, mMobileNumber, mPassword;

    UserEditTask(String firstName, String middleName, String lastName, String mobileNumber, String email, String password) {
        mFirstName = firstName.toUpperCase();
        mMiddleName = middleName.toUpperCase();
        mLastName = lastName.toUpperCase();
        mEmail = email.toLowerCase();
        mMobileNumber = mobileNumber;
        mPassword = password;
    }
    UserDatabaseDbHelper mDbHelper=new UserDatabaseDbHelper(getApplicationContext());
    @Override
    protected Boolean doInBackground(Void... voids) {
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(UserDatabaseContract.UserDetails.COLUMN_NAME_FIRST_NAME, mFirstName);
        values.put(UserDatabaseContract.UserDetails.COLUMN_NAME_MIDDLE_NAME, mMiddleName);
        values.put(UserDatabaseContract.UserDetails.COLUMN_NAME_LAST_NAME, mLastName);
        values.put(UserDatabaseContract.UserDetails.COLUMN_NAME_MOBILE_NUMBER, mMobileNumber);
        values.put(UserDatabaseContract.UserDetails.COLUMN_NAME_PASSWORD, mPassword);
        db.update(UserDatabaseContract.UserDetails.TABLE_NAME,values, UserDatabaseContract.UserDetails.COLUMN_NAME_EMAIL+" = ?",
                new String[] {String.valueOf(mEmail)});
        db.close();
        mDbHelper.close();

        return true;
    }

    @Override
    protected void onPostExecute(Boolean success) {
       if(success){
           SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
           SharedPreferences.Editor editor = pref.edit();
           if(pref.getBoolean("remember",false)) {
               editor.putString("email", mEmail);
               editor.putString("password", mPassword);
               editor.commit();
           }
           Intent intent=new Intent(getApplicationContext(),LoginActivity.class);
           intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
           intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
           startActivity(intent);
           finish();
       }
    }
}

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }
    private void attemptEdit() {
        if(mEditTask!=null){
            return;
        }
        mFirstName.setError(null);
        mMiddleName.setError(null);
        mLastName.setError(null);
        mMobileNumber.setError(null);

        mPassword.setError(null);

        String firstName=mFirstName.getText().toString().trim();
        String middleName=mMiddleName.getText().toString().trim();
        String lastName=mLastName.getText().toString().trim();
        String mobileNumber=mMobileNumber.getText().toString().trim();
       String email=mPrevEmail;
        String password=mPassword.getText().toString().trim();
        boolean cancel=false;
        View focusView=null;

        if(TextUtils.isEmpty(password)){
            cancel=true;
            focusView=mPassword;
            mPassword.setError("This field is required");
        }
        if(password.length()<6){
            cancel=true;
            focusView=mPassword;
            mPassword.setError("Password must be at least six characters");
        }
        if(TextUtils.isEmpty(mobileNumber)){
            cancel=true;
            focusView=mMobileNumber;
            mMobileNumber.setError("This field is required");
        }
        if(TextUtils.isEmpty(lastName)){
            cancel=true;
            focusView=mLastName;
            mLastName.setError("This field is required");
        }
        if(TextUtils.isEmpty(firstName)){
            cancel=true;
            focusView=mFirstName;
            mFirstName.setError("This field is required");
        }
        if(cancel){
            focusView.requestFocus();
        }else{
            showProgress(true);
            mEditTask=new UserEditTask(firstName,middleName,lastName,mobileNumber,email,password);
            mEditTask.execute((Void[]) null);
        }
    }
}
