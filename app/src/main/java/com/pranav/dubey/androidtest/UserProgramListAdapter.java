

package com.pranav.dubey.androidtest;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

/**
 * Created by dubeypranav4 on 19-03-2017.
 */
public class UserProgramListAdapter extends BaseAdapter {

    AttendanceList attList = new AttendanceList();
    Context context;
    Cursor c;
    int size;
    Boolean change = false;
    String[] options = {"----", "ABSENT", "PRESENT", "SICK"};
    Boolean[] addNotePressed;
    int[] values;
    String[] AddNoteValue;
private final class ViewHolder{
        public TextView programName;
        public Spinner attendanceSpinner;
        public TextView attendanceStatus;
}
    int flag=0;
    public UserProgramListAdapter(Context cont, Cursor cursor,int sz) {
        super();
        context = cont;
        c=cursor;

        size=sz;
        values=new int[size];
        addNotePressed=new Boolean[size];
        AddNoteValue=new String[size];
        for(int i=0;i<size;i++){
            values[i]=0;
            AddNoteValue[i]="----";
            addNotePressed[i]=false;
        }
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {
        View row=convertView;
        final ViewHolder viewHolder;
        if (row==null){
            row=LayoutInflater.from(context).inflate(R.layout.user_program_list_view_row_layout,null);
            viewHolder=new ViewHolder();
            viewHolder.programName= (TextView) row.findViewById(R.id.row_user_program_list_textView);
            viewHolder.attendanceSpinner= (Spinner) row.findViewById(R.id.row_user_program_list_spinner);
            viewHolder.attendanceStatus= (TextView) row.findViewById(R.id.attendanceStatus);
            row.setTag(viewHolder);
        }
        else{
            viewHolder= (ViewHolder) row.getTag();
        }
        c.moveToPosition(position);

        viewHolder.programName.setText(c.getString(c.getColumnIndexOrThrow(c.getColumnName(1))));
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(context, R.layout.spinner_item, options);
        viewHolder.attendanceSpinner.setAdapter(arrayAdapter);
        final View finalRow = row;

        viewHolder.attendanceSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, final View view, int i, long l) {
                values[position] = i;




                        AddNoteValue[position] = options[i];
                        viewHolder.attendanceStatus.setText(AddNoteValue[position]);
                        if(viewHolder.attendanceStatus.getText().toString().equalsIgnoreCase(options[0])){

                            Log.d("1", "removing data");

                             attList.removeData(viewHolder.programName.getText().toString().trim());
                             attList.seeData();

                        }else{

                               Log.d("1", "entering data");
                                String str=attList.getItemIndex(viewHolder.programName.getText().toString().trim());
                            if(str.equals(viewHolder.attendanceStatus.getText().toString())){

                            }
                            else{
                                attList.removeData(viewHolder.programName.getText().toString().trim());
                                attList.enterData(viewHolder.programName.getText().toString().trim(), viewHolder.attendanceStatus.getText().toString());
                                attList.seeData();
                            }


                        }



            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

       viewHolder.attendanceSpinner.setSelection(values[position]);
        viewHolder.attendanceStatus.setText(AddNoteValue[position]);

return  row;
    }


    @Override
    public int getCount() {
        return c.getCount();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    public AttendanceList getAttendanceList(){
        return attList;
    }

}

