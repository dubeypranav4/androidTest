package com.pranav.dubey.androidtest;

import android.util.Log;

import java.util.ArrayList;

/**
 * Created by dubeypranav4 on 20-03-2017.
 */
public class AttendanceList {
    private ArrayList<String> programNameArray=new ArrayList<String>();
    private ArrayList<String> attendanceArray=new ArrayList<String>();

    public void enterData(String programName,String attendance){
        Log.d("values coming",programName+" "+attendance);

            for (int i = 0; i < programNameArray.size(); i++) {
                if ((programNameArray.get(i).contains(programName))) {
                    if (attendanceArray.get(i).contains(attendance))
                        Log.d("1", "1");
                    return;
                }
                if (programNameArray.get(i).equalsIgnoreCase(programName) ) {
                   if(!(attendanceArray.get(i).toString().equals(attendance))) {
                       Log.d("ghusa","ghusa");
                        removeData(programNameArray.get(i));
                       enterData(programName,attendance);
                        Log.d("1", "2");
                   }


                    return;
                }
                if (!(programNameArray.get(i).equalsIgnoreCase(programName)) && !(attendanceArray.get(i).equalsIgnoreCase(attendance))) {
                    programNameArray.add(programName);
                    attendanceArray.add(attendance);
                    Log.d("1", "3");

                    return;
                }



        }
        programNameArray.add(programName);
        attendanceArray.add(attendance);

    }
    public void removeData(String programName){
        for(int i=0;i<programNameArray.size();i++){
            if(programNameArray.get(i).equalsIgnoreCase(programName)){
                programNameArray.remove(i);
                attendanceArray.remove(i);

            }
        }
    }
    public void seeData(){
        if (attendanceArray.isEmpty()){
            Log.d("program name","empty");
        }
        for(int i=0;i<programNameArray.size();i++){
            Log.d("programName",programNameArray.get(i)+"->"+attendanceArray.get(i)+" "+programNameArray.size()+" "+attendanceArray.size());

        }
    }
    public String getItemIndex(String programName){
        for (int i=0;i<programNameArray.size();i++){
            if(programNameArray.get(i).equals(programName)){
                return  attendanceArray.get(i);
            }
        }
        return "NOT";
    }
    public int size(){
        return programNameArray.size();
    }
    public String getNameAtI(int i)
    {
        String name= programNameArray.get(i);
        name=name.trim();
        name=name.replace(" ","_");
        return name;
    }
    public String getAttendanceAtI(int i){
        return attendanceArray.get(i);
    }

}
