package com.pranav.dubey.androidtest;

import android.content.Context;
import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CursorAdapter;
import android.widget.RadioButton;
import android.widget.TextView;

import java.util.ArrayList;
import com.pranav.dubey.androidtest.UserDatabaseContract.UserDetails;

/**
 * Created by dubeypranav4 on 18-03-2017.
 */

public class CustomArrayAdapter extends BaseAdapter {
   private AttendeesList attendeesList=new AttendeesList();
Context context;
    Cursor c;
    int size;
Boolean[] cbSelection;
    private final class ViewHolder {
        public TextView Name;
        public TextView Email;
        public CheckBox checkBox;
    }
        public CustomArrayAdapter(Context cont,Cursor cursor,int sz)
        {

            super();
            context=cont;
            c=cursor;
            size=sz;
            cbSelection=new Boolean[size];
            for(int i=0;i<size;i++){
                cbSelection[i]=false;
            }
        }


 /*   @Override
    public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
        return LayoutInflater.from(context).inflate(R.layout.row_layout,viewGroup,false);
    }*/


    @Override
    public int getCount() {
        return c.getCount();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
              View row=convertView;
        final ViewHolder viewHolder;
        if (row==null){
            row=LayoutInflater.from(context).inflate(R.layout.row_layout,null);
            viewHolder=new ViewHolder();
            viewHolder.Name= (TextView) row.findViewById(R.id.row_name);
            viewHolder.Email= (TextView) row.findViewById(R.id.row_email);
            viewHolder.checkBox= (CheckBox) row.findViewById(R.id.row_check_box);
            row.setTag(viewHolder);
            
        }
       else{
           viewHolder= (ViewHolder) row.getTag();
        }

            c.moveToPosition(position);
            String name;
            String firstName=c.getString(c.getColumnIndexOrThrow(UserDetails.COLUMN_NAME_FIRST_NAME));
            String middleName=c.getString(c.getColumnIndexOrThrow(UserDetails.COLUMN_NAME_MIDDLE_NAME));
            String lastName=c.getString(c.getColumnIndexOrThrow(UserDetails.COLUMN_NAME_LAST_NAME));

            if(TextUtils.isEmpty(middleName)){
                name=firstName+ " "+lastName;
            }
            else{
                name=firstName+" "+middleName+" "+lastName;
            }


            viewHolder.Name.setText(name);
            viewHolder.Email.setText(c.getString(c.getColumnIndexOrThrow(UserDetails.COLUMN_NAME_EMAIL)));
            viewHolder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if (b) {
                        cbSelection[position] = true;
                        add(viewHolder.Email.getText().toString());
                    } else {
                        cbSelection[position] = false;
                        remove(viewHolder.Email.getText().toString());
                    }
                }
            });

            viewHolder.checkBox.setChecked(cbSelection[position]);
        return row;



    }

  /*  @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView mName= (TextView) view.findViewById(R.id.row_name);
        TextView mEmail= (TextView) view.findViewById(R.id.row_email);

        final String firstName,middleName,lastName,email,name;

        email=cursor.getString(cursor.getColumnIndexOrThrow(UserDatabaseContract.UserDetails.COLUMN_NAME_EMAIL));
        firstName=cursor.getString(cursor.getColumnIndexOrThrow(UserDatabaseContract.UserDetails.COLUMN_NAME_FIRST_NAME));
        middleName=cursor.getString(cursor.getColumnIndexOrThrow(UserDatabaseContract.UserDetails.COLUMN_NAME_MIDDLE_NAME));
        lastName=cursor.getString(cursor.getColumnIndexOrThrow(UserDatabaseContract.UserDetails.COLUMN_NAME_LAST_NAME));

        if(TextUtils.isEmpty(middleName)){
            name=firstName+ " "+lastName;
        }
        else{
            name=firstName+" "+middleName+" "+lastName;
        }

        mName.setText(name);
        mEmail.setText(email);

        CheckBox checkBox= (CheckBox) view.findViewById(R.id.row_check_box);
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                            add(email);

                }
                else {
                    remove(email);
                }
            }
        });


    }*/

    private void remove(String email) {
        attendeesList.removeData(email);
        attendeesList.seeData();
    }

    private void add(String email) {
        attendeesList.enterData(email);
        attendeesList.seeData();
    }
    public AttendeesList getRegisteredAttendees(){
        return attendeesList;

    }
}
