package com.pranav.dubey.androidtest;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.pranav.dubey.androidtest.UserDatabaseContract.UserDetails;

import java.util.ArrayList;

public class UserProgramList extends AppCompatActivity {


    private View mLoginFormView;
    private View mProgressView;
    private GetUserData mGetUserDataTask = null;
    private SendAttendance mSendAttTask=null;
    private ViewFlipper viewFlipper;

    TextView programName1,programName2;
    ArrayList<String> programnames;
    ArrayList<String> attendances;
    EditText et1,et2;
    RadioGroup rg1,rg2;
    RadioButton present1,absent1,sick1,addNote1;
    RadioButton present2,absent2,sick2,addNote2;

    Button send1,send2,skip1,skip2;
    String attendance1,attendance2,programname1,programname2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_program_list);

        mLoginFormView = findViewById(R.id.user_program_form);
        mProgressView = findViewById(R.id.user_program_list_progress);

        et2 = (EditText) findViewById(R.id.Add_Note_2);
        rg2 = (RadioGroup) findViewById(R.id.radio_group_2);
        present2 = (RadioButton) findViewById(R.id.present_2);
        absent2 = (RadioButton) findViewById(R.id.absent_2);
        sick2 = (RadioButton) findViewById(R.id.sick_2);
        addNote2 = (RadioButton) findViewById(R.id.add_note_2);

        programnames = new ArrayList<String>();
        attendances = new ArrayList<String>();

        send1 = (Button) findViewById(R.id.send_attendance_button_1);
        skip1 = (Button) findViewById(R.id.cancel_attendance_button_1);

        send2 = (Button) findViewById(R.id.send_attendance_button_2);
        skip2 = (Button) findViewById(R.id.cancel_attendance_button_2);
        et1 = (EditText) findViewById(R.id.Add_Note_1);
        et1.setVisibility(View.INVISIBLE);
        et2.setVisibility(View.INVISIBLE);
        programName1 = (TextView) findViewById(R.id.programName_1);
        programName2 = (TextView) findViewById(R.id.programName_2);
        rg1 = (RadioGroup) findViewById(R.id.radio_group_1);
        present1 = (RadioButton) findViewById(R.id.present_1);

        absent1 = (RadioButton) findViewById(R.id.absent_1);
        sick1 = (RadioButton) findViewById(R.id.sick_1);
        addNote1 = (RadioButton) findViewById(R.id.add_note_1);


        viewFlipper= (ViewFlipper) findViewById(R.id.flipper);
        viewFlipper.setInAnimation(this, android.R.anim.fade_in);
        viewFlipper.setOutAnimation(this, android.R.anim.fade_out);
        showProgress(true);
        mGetUserDataTask = new GetUserData(getIntent().getStringExtra(UserDatabaseContract.UserDetails.COLUMN_NAME_EMAIL));
        mGetUserDataTask.execute((Void[]) null);


    }

    public class GetUserData extends AsyncTask<Void, Void, Cursor> {
        private final String mEmail;

        GetUserData(String email) {
            mEmail = email;
        }

        UserDatabaseDbHelper mDbHelper = new UserDatabaseDbHelper(getApplicationContext());

        @Override
        protected Cursor doInBackground(Void... voids) {
            SQLiteDatabase db = mDbHelper.getReadableDatabase();
            String query = "SELECT * FROM " + UserDetails.TABLE_NAME + " WHERE " + UserDetails.COLUMN_NAME_EMAIL + " = \'" + mEmail + "\'";
            Cursor cursor = null;
            MatrixCursor newCursor = null;
            try {
                cursor = db.rawQuery(query, null);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();


                try {
                    String[] columnName = new String[]{UserDetails.COLUMN_ID, UserDetails.PROGRAM_NAME};
                    newCursor = new MatrixCursor(columnName);
                    int j=0;
                    for (int i = 7; i < cursor.getColumnCount(); i++) {
                        Log.d("cursor data", cursor.getColumnName(i) + "->" + cursor.getString(i));
                        if ((cursor.getString(i).equals(UserDetails.SELECTED_ATTENDEE_VALUE))) {
                            String name=cursor.getColumnName(i).replace("_"," ");
                           newCursor.addRow(new Object[]{j,name});
                            j++;
                        }
                    }


                    newCursor.moveToFirst();

                } catch (Exception e) {
                    e.printStackTrace();
                }


                return newCursor;
            } else {
                return null;
            }

        }

        @Override
        protected void onPostExecute(Cursor cursor) {
            showProgress(false);
            mGetUserDataTask = null;
            Log.d("new Cursor Data", cursor.getColumnName(0) + cursor.getColumnName(1));
            if (cursor.getCount()>0) {
             /*   do{
                    Log.d("new Cursor Data",cursor.getString(cursor.getColumnIndexOrThrow(cursor.getColumnName(1))));
                }while(cursor.moveToNext());*/
                cursor.moveToFirst();
                programName1.setText(cursor.getString(1));
                populateUserProgramList(cursor);
            } else {
                programName1.setText("No Attendances to give");
                et1.setVisibility(View.GONE);
                rg1.setVisibility(View.GONE);
                send1.setVisibility(View.GONE);
                skip1.setVisibility(View.GONE);

                programName2.setText("No Attendances to give");
                et2.setVisibility(View.GONE);
                rg2.setVisibility(View.GONE);
                send2.setVisibility(View.GONE);
                skip2.setVisibility(View.GONE);
            }
        }
        private void check(int i) {
            if(i==1){
                if(present1.isChecked()){
                    attendance1="PRESENT";
                    return;
                }
                if (absent1.isChecked()){
                    attendance1="ABSENT";
                }
                if (sick1.isChecked()){
                    attendance1="SICK";
                }
            }
            else{
                if (present2.isChecked()){
                    attendance2="PRESENT";
                }
                if (absent2.isChecked()){
                    attendance2="ABSENT";
                }
                if (sick2.isChecked()){
                    attendance2="SICK";
                }
            }
        }
        public void showNothing() {

            programName1.setText("No Attendances to give");
            et1.setVisibility(View.GONE);
            rg1.setVisibility(View.GONE);
            send1.setVisibility(View.GONE);
            skip1.setVisibility(View.GONE);

            programName2.setText("No Attendances to give");
            et2.setVisibility(View.GONE);
            rg2.setVisibility(View.GONE);
            send2.setVisibility(View.GONE);
            skip2.setVisibility(View.GONE);
            if (attendances.size()>0){
                showProgress(true);
                mSendAttTask=new SendAttendance(getIntent().getStringExtra(UserDetails.COLUMN_NAME_EMAIL));
                mSendAttTask.execute((Void[]) null);
            }
            for(int i=0;i<attendances.size();i++){
                Log.d("values",programnames.get(i)+"->"+attendances.get(i)+" "+programnames.size()+" "+attendances.size());
            }


        }
        private void populateUserProgramList(final Cursor cursor) {
            if (cursor.getCount()>0){
                present2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                        if (b) {
                            attendance2 = "PRESENT";
                        } else {
                            attendance2 = null;
                        }
                    }
                });
                absent2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                        if (b) {
                            attendance2 = "ABSENT";
                        } else {
                            attendance2 = null;
                        }
                    }
                });
                sick2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                        if (b) {
                            attendance2 = "SICK";
                        } else {
                            attendance2 = null;
                        }
                    }
                });
                addNote2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                        if (b) {
                            et2.setVisibility(View.VISIBLE);
                            et2.requestFocus();
                        } else {

                            et2.setVisibility(View.INVISIBLE);

                        }
                    }
                });

                present1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                        if (b) {
                            attendance1 = "PRESENT";
                        } else {
                            attendance1 = null;
                        }
                    }
                });
                absent1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                        if (b) {
                            attendance1 = "ABSENT";
                        } else {
                            attendance1 = null;
                        }
                    }
                });
                sick1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                        if (b) {
                            attendance1 = "SICK";
                        } else {
                            attendance1 = null;
                        }
                    }
                });
                addNote1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                        if (b) {
                            et1.setVisibility(View.VISIBLE);
                            et1.requestFocus();
                        } else {

                            et1.setVisibility(View.INVISIBLE);

                        }
                    }
                });
                send1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Boolean etVisible=false;
                        check(1);
                        Toast.makeText(getApplicationContext(),"Send 1 "+send1.getId(),Toast.LENGTH_SHORT).show();
                        if (et1.getVisibility()==View.VISIBLE){
                            attendance1=et1.getText().toString().trim();
                            etVisible=true;
                        }else{
                            etVisible=false;
                        }
                        if (cursor.getPosition() < cursor.getCount()) {

                            if(TextUtils.isEmpty(attendance1)){
                                if(etVisible) {
                                    et1.setError("Add note can not be empty");
                                    et1.requestFocus();

                                }else{
                                    Toast.makeText(getApplicationContext(),"Select one of the attendance options or skip",Toast.LENGTH_LONG).show();
                                }
                            }else {
                                programnames.add(programName1.getText().toString());
                                attendances.add(attendance1);

                                if (cursor.getPosition() < (cursor.getCount() - 1)) {
                                    cursor.moveToNext();
                                    programName2.setText(cursor.getString(1));
                                    et2.setText("");

                                    //  present2.setChecked(false);
                                    //absent2.setChecked(false);
                                    //sick2.setChecked(false);
                                    attendance2="";
                                    //addNote2.setChecked(false);
                                    viewFlipper.showNext();
                                } else {
                                    showNothing();
                                }
                            }
                        } else {
                            Log.d("send1", "else mein ghusa");
                            showNothing();
                        }
                    }
                });
                send2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        check(2);
                        Boolean etVisible=false;
                        Toast.makeText(getApplicationContext(),"SEnd 2"+send2.getId(),Toast.LENGTH_SHORT).show();
                        if(et2.getVisibility()==View.VISIBLE){
                            attendance2=et2.getText().toString().trim();
                            etVisible=true;
                        }else{
                            etVisible=false;
                        }
                        if (cursor.getPosition() < cursor.getCount()) {
                            if(TextUtils.isEmpty(attendance2)){
                                if(etVisible) {
                                    et2.setError("Add note must not be empty");
                                    et2.requestFocus();
                                }else{
                                    Toast.makeText(getApplicationContext(),"Select one of the attendance options or skip",Toast.LENGTH_LONG).show();

                                }
                            }else {
                                programnames.add(programName2.getText().toString());
                                attendances.add(attendance2);

                                if (cursor.getPosition() < (cursor.getCount() - 1)) {
                                    cursor.moveToNext();
                                    programName1.setText(cursor.getString(1));
                                    et1.setText("");
                                    // present1.setChecked(false);
                                    // absent1.setChecked(false);
                                    // sick1.setChecked(false);
                                    attendance1="";
                                    //addNote1.setChecked(false);
                                    viewFlipper.showPrevious();
                                } else {
                                    showNothing();

                                }
                            }
                        }else{
                            Log.d("send2", "else mein ghusa");
                            showNothing();
                        }
                    }
                });
                skip2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (cursor.getPosition() < (cursor.getCount() - 1)) {
                            cursor.moveToNext();
                            programName1.setText(cursor.getString(1));
                            et1.setText("");
                            present1.setChecked(false);
                            absent1.setChecked(false);
                            sick1.setChecked(false);
                            addNote1.setChecked(false);
                            viewFlipper.showNext();
                        } else {
                            showNothing();
                        }
                    }
                });
                skip1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (cursor.getPosition() < (cursor.getCount() - 1)) {
                            cursor.moveToNext();
                            programName2.setText(cursor.getString(0));
                            et2.setText("");
                            present2.setChecked(false);
                            absent2.setChecked(false);
                            sick2.setChecked(false);
                            addNote2.setChecked(false);
                            viewFlipper.showNext();
                        } else {
                            showNothing();
                        }
                    }
                });
            }else {
                showNothing();
            }
        /*    final UserProgramListAdapter userProgramListAdapter = new UserProgramListAdapter(UserProgramList.this, cursor,cursor.getCount());

            Log.d("size", "" + cursor.getCount());


            mUserProgramListView.setAdapter(userProgramListAdapter);
            mUserProgramListView.setDescendantFocusability(ViewGroup.FOCUS_BLOCK_DESCENDANTS);
                final AttendanceList attendanceList=userProgramListAdapter.getAttendanceList();
            sendAttendance.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (attendanceList.size()!=0){
                        showProgress(true);
                        mSendAttTask=new SendAttendance(getIntent().getStringExtra(UserDetails.COLUMN_NAME_EMAIL),attendanceList);
                        mSendAttTask.execute((Void[]) null);
                    }
                }
            });

*/


        }

    }
public class SendAttendance extends AsyncTask<Void,Void,Integer>{

    String email;
    SendAttendance(String emailNew){

        email=emailNew;
    }
    UserDatabaseDbHelper mDbHelper = new UserDatabaseDbHelper(getApplicationContext());

    @Override
    protected Integer doInBackground(Void... voids) {
        int rowsAffected=-1;
        SQLiteDatabase db=mDbHelper.getWritableDatabase();
            for (int i=0;i<programnames.size();i++){
                ContentValues values=new ContentValues();
                values.put(programnames.get(i),attendances.get(i));
                String selection=UserDetails.COLUMN_NAME_EMAIL+" LIKE ?";
                String[] selectionArgs={email};
                rowsAffected=db.update(UserDetails.TABLE_NAME,
                        values,
                        selection,
                        selectionArgs);
            }
        return rowsAffected;
    }

    @Override
    protected void onPostExecute(Integer integer) {
        showProgress(false);
        mSendAttTask=null;
        if(integer==-1){
            Toast.makeText(getApplicationContext(), "Failure", Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(getApplicationContext(),"Rows affected "+integer,Toast.LENGTH_SHORT).show();
        }
        finish();
    }
}
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }


}
