package com.pranav.dubey.androidtest;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.w3c.dom.Text;
import com.pranav.dubey.androidtest.UserDatabaseContract.UserDetails;

public class UserRegistration extends AppCompatActivity {
    Boolean can=false;
    EditText mFirstName, mMiddleName, mLastName, mMobileNumber, mPassword;
    public EditText mEmail;
    Button mRegister;
    private View mProgressView;
    private View mLoginFormView;
    private UserRegistrationTask mRegTask = null;
public UserRegistration activity;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_registration);
activity=this;
        mFirstName = (EditText) findViewById(R.id.firstName);
        mMiddleName = (EditText) findViewById(R.id.middleName);
        mLastName = (EditText) findViewById(R.id.lastName);
        mEmail = (EditText) findViewById(R.id.registration_email);
        mMobileNumber = (EditText) findViewById(R.id.registration_mobile_number);
        mPassword = (EditText) findViewById(R.id.registration_password);
        mRegister = (Button) findViewById(R.id.registration_button);
        mProgressView = findViewById(R.id.registration_progress);
        mLoginFormView = findViewById(R.id.registration_form);

        mRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                mgr.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                attemptRegister();
            }
        });
    }

    public class UserRegistrationTask extends AsyncTask<Void, Void, Boolean> {
        private final String mFirstName, mMiddleName, mLastName, mEmail, mMobileNumber, mPassword;

        UserRegistrationTask(String firstName, String middleName, String lastName, String mobileNumber, String email, String password) {
            mFirstName = firstName.toUpperCase();
            mMiddleName = middleName.toUpperCase();
            mLastName = lastName.toUpperCase();
            mEmail = email.toLowerCase();
            mMobileNumber = mobileNumber;
            mPassword = password;
        }
        UserDatabaseDbHelper mDbHelper=new UserDatabaseDbHelper(getApplicationContext());

Boolean cancel=false;

        @Override
        protected Boolean doInBackground(Void... voids) {
            SQLiteDatabase db1=mDbHelper.getReadableDatabase();
            String[] projection={
                     UserDetails.COLUMN_NAME_FIRST_NAME,
                    UserDetails.COLUMN_NAME_EMAIL
            };
            String selection=UserDetails.COLUMN_NAME_EMAIL + " LIKE ?";
            String[] selectionArgs={String.valueOf(mEmail)};
            Cursor c=db1.query(
                    UserDetails.TABLE_NAME,
                    projection,
                    selection,
                    selectionArgs,
                    null,
                    null,
                    null
            );
            if(c.getCount()>0){
                cancel=true;
            }
            //inserting data
            if(!cancel) {
                SQLiteDatabase db = mDbHelper.getWritableDatabase();
                ContentValues values = new ContentValues();
                values.put(UserDetails.COLUMN_NAME_FIRST_NAME, mFirstName);
                if (!TextUtils.isEmpty(mMiddleName)) {
                    values.put(UserDetails.COLUMN_NAME_MIDDLE_NAME, mMiddleName);
                }
                values.put(UserDetails.COLUMN_NAME_LAST_NAME, mLastName);
                values.put(UserDetails.COLUMN_NAME_MOBILE_NUMBER, mMobileNumber);
                values.put(UserDetails.COLUMN_NAME_EMAIL, mEmail);
                values.put(UserDetails.COLUMN_NAME_PASSWORD, mPassword);
                db.insert(UserDetails.TABLE_NAME, UserDetails.COLUMN_NAME_MIDDLE_NAME, values);
                db.close();
                mDbHelper.close();

                return true;
            }
            else{
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean success) {

            if(success){
                Toast.makeText(getApplicationContext(),"Done",Toast.LENGTH_LONG).show();
                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                intent.putExtra("called Activity", true);
                intent.putExtra("email", mEmail);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }else{
                AlertDialog.Builder builder=new AlertDialog.Builder(UserRegistration.this);
                builder.setMessage("Do you want to login instead?")
                       .setTitle("Username already exists");
                builder.setPositiveButton("Sure",
                        new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        onClickText(null);
                    }
                });
                builder.setNegativeButton("Nope", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        doThis();
                    }
                });
                AlertDialog alertDialog=builder.create();
                alertDialog.show();
            }
            showProgress(false);
            mRegTask=null;

        }

        @Override
        protected void onCancelled() {
            mRegTask = null;
            showProgress(false);
        }
    }

    private void doThis() {
        can=true;
        mEmail.setError("Username already exists.");
        mEmail.requestFocus();
    }


    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    private void attemptRegister() {
        if (mRegTask!=null){
            return;
        }
        //Reset Errors
        mFirstName.setError(null);
        mMiddleName.setError(null);
        mLastName.setError(null);
        mMobileNumber.setError(null);
        mEmail.setError(null);
        mPassword.setError(null);

        String firstName=mFirstName.getText().toString().trim();
        String middleName=mMiddleName.getText().toString().trim();
        String lastName=mLastName.getText().toString().trim();
        String mobileNumber=mMobileNumber.getText().toString().trim();
        String email=mEmail.getText().toString().trim();
        String password=mPassword.getText().toString().trim();
        boolean cancel=false;
        View focusView=null;


        if(TextUtils.isEmpty(password)){
            cancel=true;
            focusView=mPassword;
            mPassword.setError("This field is required");
        }
        if(password.length()<6){
            cancel=true;
            focusView=mPassword;
            mPassword.setError("Password must be at least six characters");
        }
        if(TextUtils.isEmpty(email)){
            cancel=true;
            focusView=mEmail;
            mEmail.setError("This field is required");
        }
        if(TextUtils.isEmpty(mobileNumber)){
            cancel=true;
            focusView=mMobileNumber;
            mMobileNumber.setError("This field is required");
        }
        if(TextUtils.isEmpty(lastName)){
            cancel=true;
            focusView=mLastName;
            mLastName.setError("This field is required");
        }
        if(TextUtils.isEmpty(firstName)){
            cancel=true;
            focusView=mFirstName;
            mFirstName.setError("This field is required");
        }


        if((!email.contains("@"))||(!email.contains("."))){
            cancel=true;
            focusView=mEmail;
            mEmail.setError("Email address is invalid");
        }

        if(cancel){
            focusView.requestFocus();
        }else{
            showProgress(true);
            mRegTask=new UserRegistrationTask(firstName,middleName,lastName,mobileNumber,email,password);
            mRegTask.execute((Void[]) null);
        }

    }

    public void onClickText(View view) {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();

    }
}
