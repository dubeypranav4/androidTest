package com.pranav.dubey.androidtest;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.pranav.dubey.androidtest.UserDatabaseContract.UserDetails;

/**
 * Created by dubeypranav4 on 17-03-2017.
 */
public class UserDatabaseDbHelper extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "UserDatabase.db";

    private static final String T = " TEXT";
    private static final String C = " ,";


    private static final String SQL_CREATE_ENTRIES=
            "CREATE TABLE "+ UserDetails.TABLE_NAME+"("+
                    UserDetails.COLUMN_ID+" INTEGER PRIMARY KEY AUTOINCREMENT ,"+
                    UserDetails.COLUMN_NAME_FIRST_NAME+T+C+
                    UserDetails.COLUMN_NAME_MIDDLE_NAME+T+C+
                    UserDetails.COLUMN_NAME_LAST_NAME+T+C+
                    UserDetails.COLUMN_NAME_MOBILE_NUMBER+T+C+
                    UserDetails.COLUMN_NAME_EMAIL+T+C+
                    UserDetails.COLUMN_NAME_PASSWORD+T+")";

    private static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + UserDetails.TABLE_NAME;

    public UserDatabaseDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

    }


    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
sqLiteDatabase.execSQL(SQL_CREATE_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        super.onDowngrade(db, oldVersion, newVersion);
    }
}
